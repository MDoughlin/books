import { StatusBar } from "expo-status-bar";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { ApolloClient, InMemoryCache, ApolloProvider } from "@apollo/client";

import useCachedResources from "./hooks/useCachedResources";
import useColorScheme from "./hooks/useColorScheme";
import Navigation from "./navigation";

const APIKEY = "imzouren::stepzen.net+1000::6787c367d983534217bc918f56493465af2e23ab0312a3a889957b8bbbde15b8  "

const client = new ApolloClient({
  uri: "https://imzouren.stepzen.net/api/zeroed-liger/__graphql",
  headers: {
    Authorization: `Apikey ${APIKEY}`,
  },
  cache: new InMemoryCache(),
})

export default function App() {
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <SafeAreaProvider>
        <ApolloProvider client={client}>
          <Navigation colorScheme={colorScheme} />
        </ApolloProvider>
        <StatusBar />
      </SafeAreaProvider>
    )
  }
}
